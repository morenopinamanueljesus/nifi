<img src="https://thecustomizewindows.com/wp-content/uploads/2017/07/How-To-Install-Apache-NiFi-On-Ubuntu-16.04-LTS.png" width="900" height="400" alt="Apache NiFi"/>

# Tabla de Contenidos

- [Java](#java)
- [Maven](#maven)
- [Nifi](#nifi)
- [QuickStart](#quickStart)


# Java
**Instalamos Java en nuestro sistema:**

**Añadimos repositorios:**
```bash
sudo add-apt-repository -y ppa:webupd8team/java
```

**Actualizamos el sistema:**
```bash
sudo apt-get update
```

**Instalamos java:**
```bash
sudo apt-get install -y oracle-java8-installer
```

**Vemos la versión de java:**
```bash
java -version
```

**Nos devolverá lo siguiente:**
```bash
java version "1.8.0_201"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)
```



**Reiniciamos el sistema:**
```bash
sudo reboot
```

# Maven
**Actualizamos el sistema:**
```bash
sudo apt update
```

**Instalamos Maven:**
```bash
sudo apt install maven
```

**Comprobamos la instalación mirando la versión:**
```bash
mvn -v
```

**Nos mostrará lo siguiente:**
```bash
Apache Maven 3.5.2
Maven home: /usr/share/maven
Java version: 1.8.0_201, vendor: Oracle Corporation
Java home: /usr/lib/jvm/java-8-oracle/jre
Default locale: es_ES, platform encoding: UTF-8
OS name: "linux", version: "4.15.0-29-generic", arch: "amd64", family: "unix"
```

# Nifi
**Descargamos el paquete de Nifi, para ello, necesitamos la url, donde se encuentra los archivos binarios de Nifi:**
```bash
https://nifi.apache.org/download.html
```
<img src="images/install-nifi-1.PNG" width="750" height="400" alt="Apache NiFi"/>

**Visualizamos la última versión de Nifi, en el apartado Releases, en este caso, 1.9.0.**
**Hacemos clic en el apartado Binaries, con nombre de archivo "nifi-x.x.x-bin.tar.gz".**

**Visualizamos el apartado HTTP, y copiamos la url.**

<img src="images/install-nifi-2.PNG" width="750" height="400" alt="Apache NiFi"/>

**En el terminal de nuestra máquina:**
**Nos desplazamos al directorio home**
```bash
cd ~
```

**Descargamos el pquete comprimido de Nifi:**
```bash
wget http://apache.rediris.es/nifi/x.x.x/nifi-x.x.x-bin.tar.gz
```

**Descomprimimos:**
```bash
tar xf nifi-x.x.x-bin.tar.gz
```

**Nos desplazamos al directorio bin de Nifi:**
```bash
cd nifi-x.x.x/bin
```

**Ejecutamos el script, para instalar el servicio de Nifi:**
```bash
sudo ./nifi.sh install
```

**Iniciamos servicio de nifi:**
```bash
sudo ./nifi.sh start
```

# QuickStart
**Para visualizar Nifi, nos dirigimos a un navegador, e introducimos la url:**
```bash
http://direcciónIP:8080/nifi
```
<img src="images/install-nifi-3.PNG" width="750" height="400" alt="Apache NiFi"/>

