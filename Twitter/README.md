# Objetivo
En este apartado veremos como, recoger los tweets de un usuario en concreto de Twitter.

# Filosofía 
Este procesador recoge los tweets que se publiquen a nombre del perfil seleccionado. El formato de salida es JSON.

# Antes de empezar
Antes de nada, tenemos que conseguir varios datos de Twitter.
Son cuatro datos:
- Consumer Key
- Consumer Secret
- Access Token
- Access Token Secret

Estos datos son proporcionado por Tiwtter.<a href="http://codygo.es/redes-sociales/conseguir-las-consumer-key-y-access-token-de-twitter/"> Enlace para conseguir los datos de acceso a Twitter.</a>

# Nifi
### Importar Template 
Nos dirigimos a Nifi Web, para poder importar el tempale.
Descargamos el <a href=TWITTER.xml>Template</a>

Para poder importar un Template a Nifi, nos dirigimos a la zona de Operate. Si visualizamos las opciones, nos aparece una opcion, Import Template:

<img src="images/template-1.PNG"/>

Nos aparecerá una nueva ventana, en el cual, hacemos clic en la lupa, buscamos el fichero descargado, y lo importamos:

<img src="images/template-2.PNG"/>

Una vez importado, añadimos el nuevo template, para ello, arrastramos la opción de Template al tapiz:

<img src="images/template-4.PNG"/>

Seleccionamos el nuevo template que hemos importado:

<img src="images/twitter-1.PNG"/>

Nos aparecerá, todo el flujo de datos incorporado:

<img src="images/twitter-2.PNG"/>

### Configuración procesador
**GetTwitter**
- Twitter Endpoint: lenguaje de expresion del punto final, por defecto.
- Consumer Key: suministrado por Twitter.
- Consumer Secret: suministrado por Twitter.
- Access Token: suministrado por Twitter.
- Access Token Secret: suministrado por Twitter.
- Languages: lenguaje filtrado.
- Terms to Filter On: términos para filtrar.
- IDs to Follow: identificador de perfil de Twitter.
- Locations to Filter On: ubicaciones para filtrar.

<img src="images/GetTwitter.PNG"/>
