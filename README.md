<img src="https://nifi.apache.org/assets/images/apache-nifi-logo.svg" width="300" height="126" alt="Apache NiFi"/>

## Tabla de Contenidos

- [Características](#características)
- [Requisitos](#requisitos)

## Características
Apache Nifi, fué hecho para el flujo de datos. Admite gráficos dirigidos altamente configurables de enrutamiento de datos, transformación y lógica de mediación del sistema.
Algunas de sus características incluye:
- Interfaz de usuario basada en web.
- Experiencia perfecta para el diseño, control y monitoreo.
- Experiencia de usuario multiusuario.
- Altamente configurable.
- Tolerancia a la pérdida vs entrega garantizada.
- Baja latencia vs alto rendimiento.
- Priorización dinámica.
- Los flujos pueden ser modificados en tiempo de ejecución.
- Se amplía para aprovechar la capacidad total del servidor.
- Seguimiento de flujo de datos de principio a fin.
- Contruir propios procesadores.
- SSL, SSH, HTTPS,...

## Requisitos
* Sistema Operativo Linux (Ubuntu +16, CentOS,...)
* JDK 1.8 o más reciente
* Apache Maven 3.1.0 o superior
* Nifi Bin
