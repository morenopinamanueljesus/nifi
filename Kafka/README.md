<img src="https://kafka.apache.org/images/kafka_diagram.png" width="300" heigth="300"/><img src="https://nifi.apache.org/assets/images/apache-nifi-logo.svg" width="300" height="126" alt="Apache NiFi"/>

# Tabla de contenidos
- [Instalación de Apache Kafka](#instalación-de-apache-kafka)
- [Crear topic en Kafka](#crear-topic-en-kafka)
- [Configuraciones en Nifi](#configuraciones-en-nifi)


# Instalación de Apache Kafka
Primero actualizamos el caché de repositorio de paquetes del servidor de Ubuntu.
```bash
sudo apt-get update
```

Apache Kafka depende de Java. Si ya tenemos instalado Nifi, no hace falta hacer este tramo, ya que nifi también depende de Java.
```bash
sudo add-apt-repository -y ppa:webupd8team/java
```
```bash
sudo apt-get update
```
```bash
sudo apt-get install -y oracle-java8-installer
```

Ahora tenemos que instalar zookeeper. Está disponible en el repositorio oficial de paquetes de Ubuntu.
```bash
sudo apt-get install zookeeperd
```

Ejecutamos el siguiente comando para verificar si se está ejecutando zookeeper.
```bash
sudo systemctl status zookeeper
```

Si no se está ejecutando por algún motivo, iniciamos zookeeper.
```bash
sudo systemctl start zookeeper
```

Agregamos un guardian del zookeeper para el inicio del sistema. Así que se iniciará automáticamente en el arranque.
```bash
sudo systemctl enable zookeeper
```

Ahora instale el paquete net-tools.
```bash
sudo apt-get install net-tools
```

Ahora ejecutamos el siguiente comando para verificar si zookeeper se está ejecutando en el puerto 2181.
```bash
sudo netstat -tulpen | grep 2181
```

Nos desplazamos al directorio Descargas.
```bash
cd ~/Descargas
```

Ahora descargaremos Apache Kafka. En el momento de escribir este manual, la última versión de Apache Kafka es v2.1.0.
Ejecute el siguiente comando para descargar Apache Kafka 1.0.0 con wget.
```bash
wget http://apache.rediris.es/kafka/2.1.0/kafka_2.11-2.1.0.tgz 
```

Ahora creamos un directorio Kafka en el directorio opt.
```bash
sudo mkdir /opt/Kafka
```

Ahora extraiga el archivo Apache Kafka en el directorio /opt/Kafka.
```bash
sudo tar xvzf kafka_2.11-2.1.0.tgz -C /opt/Kafka
```

Modificamos /etc/profile:
```bash
sudo nano /etc/profile
```

Al final del documento, añadimos.
```bash
export KAFKA_HOME="/opt/Kafka/kafka_2.11-2.1.0/"
export PATH="$PATH:${KAFKA_HOME}/bin"
```

Ahora abra el archivo ~/.bashrc
```bash
sudo nano ~/.bashrc
```

Al final del fichero, añadimos.
```bash
alias sudo='sudo env PATH="$PATH"'
```

Reiniciamos.
```bash
sudo reboot
```

Ahora, para hacer nuestra vida más fácil, creamos un enlace simbólico del archivo server.properties de Kafka de la siguiente manera.
```bash
sudo ln -s $ KAFKA_HOME /config/server.properties /etc/kafka.properties
```

Iniciamos el servidor de Apache Kafka.
```bash
sudo kafka-server-start.sh /etc/kafka.properties
```

# Crear topic en Kafka
Para crear un topic:
```bash
sudo kafka-topics.sh --create --zookeeper localhost 2181 --replication-factor 1 --partitions 1 --topic nifi
```

Para enviar mensajes:
```bash
 sudo kafka-console-producer.sh --broker-list localhost:9092 --topic nifi
```

Una vez que presione <Intro> debería ver un nuevo signo de flecha (>). A partir de ahí, todo se categoriza como mensaje.

Para ver los mensajes que se han enviado
```bash
sudo kafka-console-consumer.sh --zookeeper localhost:2181 --topic nifi --from-beginning
```

# Configuraciones en Nifi
Nos dirigimos a la web de Nifi, para poder configurar un procesador de Kafka.
Lo que haremos es, capturar todos los mensajes que se hacen a través del topic que hemos creado, llamo nifi.

Para ello, en el tapíz de Nifi, arrastramos un procesador, en el cual nos aparece una ventana.
Seleccionamos ConsumeKafka_2_0 (2_0, corresponde a la verión de Kafka)

<img src="images/kafka.PNG"/>

Nos direigimos a las propiedades del procesador.
Configuraciones a realizar:
- Kafka Brokers: dirección IP y puerto que utiliza 
- Security Protocol: No hemos configurado ningún protocolo de seguridad, en nuestro caso PLAINTEXT
- Topic name: Nombre del topic, en nuestro caso, nifi
- Topic Name format: nos indica en que formato, seleccionamos, pattern
- Group ID: por defecto, consume-consumer

<img src="images/kafka_2.PNG"/>

Aceptamos la configuración y lanzamos el procesador para que capture datos.

Todos los mensajes escritos anteriormente, Nifi los captura.
Si vemos la relación entre ConsumeKafka y otro procesador como PutFile, vemos como transfiere datos.

Para verlo, sin iniciar PutFile, los mensajes se queda en la relación. Hacienco clic derecho sobnre la relación, y y luego en List queue, para ver que tiene en cola.

<img src="images/kafka_3.PNG"/>

Vemos un listado con los distintos mensajes que ha recogido Nifi.
Haceos clic en el icono i.

<img src="images/kafka_4.PNG"/>

Nos aparecerá una nueva ventana, en el cual hacemos clic en View.

<img src="images/kafka_5.PNG"/>

Y vemos el mensaje.

<img src="images/kafka_6.PNG"/>
