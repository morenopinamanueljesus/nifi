# Objetivo
En este apartado, veremos como importar a nuestro Nifi, un Template ya diseñado, para podercapturar cambios en una base de datos de MySQL, para luego, transformarlos en JSON, y a su vez, enviarlos por HTTP.

# Filosofía 
En este apartado, hacemos un flujo de datos, recogiendo los cambios que se producen en un servidor MySQL, y según el tipo (insert, update o delete), hacemos un tratamiento para modificar parametros en Nagan.
Nifi, hace de interconexión entre dos servidores, MySQL y Nagan.

En este ejemplo, tenemos el servidor MySQL con dirección IP 10.240.218.126, Nifi con la dirección IP 10.236.197.129, y Nagan con la dirección IP 10.240.218.69.

# Servidor MySQL (configuraciones)
Necesitamos que el servidor MySQL sea accesible desde el exterior, si es nuestro caso. Para ello, nos conectamos vías ssh al servidor MySQL (ejemplo: 10.240.218.126).
Editamos el fichero de configuración de MySQL, para que sea accesible desde la red exterior.
```bash
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
```

Comentamos esta línea:
```bash
#bind-address		= 127.0.0.1
```

Editamos el fichero de configuración:
```bash
nano /etc/mysql/my.cnf
```

Indicamos que los cambios que se hagan en la base de datos, se introduzcan en unos ficheros binlog.
```bash
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

Editamos el fichero de configuración.
```bash
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
```

Buscamos las líneas que contienen estas configuraciones, y lo editamos
```bash
server-id               = 1
log_bin                 = /var/log/mysql/mysql-bin.log
binlog_format           = row
expire_logs_days        = 10
max_binlog_size   = 100M
binlog_do_db            = alarm

```

Reiciamos la base de datos
```bash
sudo service mysql restart 
```

# Nagan (configuraciones)
```bash
Rellenar por Adrián
```

# Nifi (configuraciones)

### Instalar librería MySQL
En el servidor donde tenemos Nifi, tenemos que instalar una librería de java, que hace de conector con el servidor MySQL.
```bash
apt-get install libmysql-java
```

### Importar Template 
Nos dirigimos a Nifi Web, para poder importar el tempale.
Descargamos el <a href=MySQL-Nagan/template-mysql-naga.xml>Template</a>

Para poder importar un Template a Nifi, nos dirigimos a la zona de Operate. Si visualizamos las opciones, nos aparece una opcion, Import Template:

<img src="images/template-1.PNG"/>

Nos aparecerá una nueva ventana, en el cual, hacemos clic en la lupa, buscamos el fichero descargado, y lo importamos:

<img src="images/template-2.PNG"/>

Una vez importado, añadimos el nuevo template, para ello, arrastramos la opción de Template al tapiz:

<img src="images/template-4.PNG"/>

Seleccionamos el nuevo template que hemos importado:

<img src="images/template-3.PNG"/>

Nos aparecerá, todo el flujo de datos incorporado:

<img src="images/template-5.PNG"/>

### Detalles sobre el template
**CaptureChangeMySQL**: en este procesador, lo que conseguimos es capturar los cambios que se realizan en el servidor de MySQL.
- MySQL Hosts: Dirección IP del servidor MySQL y puerto que escucha (ejemplo: 10.240.218.126:3306).
- MySQL Driver Class Name: Por defecto, el controlador que utiliza Nifi.
- MySQL Driver Location: fichero del controlador de java para conectar con la base de datos.
- Username: usuario de la base de datos, el cual es accesible desde el exterior.
- Password: contraseña del usuario.
- Server ID: Identificador del servidor (normalmente por defecto vacío).
- Database/Schema Name Pattern: nombre de la base de datos que queremos caprturar los cambios (si queremos todas las bases de datos, dejar vacío ).
- Table Name Pattern: nombre de la tabla a capturar (si lo dejamos vacío, se refiere a todas las tablas).

 <img src="images/CaptureChangeMySQL.PNG"/>

**RouteOnAttribute**: Ruta según los atributos.
- Routing Strategy: valor predeterminado del lenguaje de expresión
- delete: esta configuración es creada, a partir del botón +, con el valor ${cdc.event.type:equals('delete')}, que captura el borrado.
- insert: esta configuración es creada, a partir del botón +, con el valor ${cdc.event.type:equals('insert')}, que captura la insercción.
- update: esta configuración es creada, a partir del botón +, con el valor ${cdc.event.type:equals('update')}, que captura las modificaciones.

<img src="images/RouteOnAttribute.PNG"/>

**GetTableName (EvaluateJsonPath)**: conseguir el nombre de la tabla que ha cambiado.
- Destionation: valor por defecto, flowfile-attribute.
- Ruturn Type: por defecto, auto-detect.
- Path Not Found Behavior: por defecto, ignore.
- Null Value Representation: por defecto, empty string.
- tableName: esta configuración es creada, a partir del botón +, con el valor $.table_name , que importamos de la variable con el nombre de la tabla que hemos capturado los cambios.

<img src="images/GetTableName.PNG"/>

**Transform to Flat JSON (JoltTransformJSON)**: transforma a JSON
- Jolt Transformation DSL: especifica la transformacion, por defecto, chain.
- Custom Transformation Class Name: nombre de clase para el alcance del lenguaje de expresión de transformación, por defecto, vacío.
- Custom Module Directory: ruta de archivos y directorios, con las tranformaciones personalizadas.
- Jolt Specification: especificación para lara la tranformación de datos JSON. 
[
  {
    "operation": "shift",
    "spec": {
      "columns": {
        "*": {
          "@(value)": "[#1].@(1,name)"
        }
      }
    }
  }
]
- Transform Cache Size: Tamaño de caché para la transformación.
- Pretty Print: formato de impresión al salida.

<img src="images/JoltTransformJSON.PNG"/>

**InvokeHTTP**: enviar contenido a Nagan
- HTTP Method: Método de solicitud HTTP.
- Remote URL: URL.
- SSL Context Service: servicio de contexto SSL..
- Connection Timeout: Tiempo máximo de espera.
- Read Timeout: Tiempo máximo de espera para respuesta.
- Include Date Header: incluir encabezado.
- Follow Redirects: seguir las redirecciones HTTP emitidas.

<img src="images/InvokeHTTP.PNG"/>

### Configurar Tapíz
Nifi, para capturar los cambios que se hagan en MySQL, necesita un servicio, que interactúa con la base de datos.
Tenemos que configurar el tapíz de nuestro Nifi, para ello, hacemos clic en cualquier zona libre de Nifi, y en el apartado Operate, en la parte izquierda de la ventana, pulsamos sobre le icono de la rueda dentada:

<img src="images/tapiz-1.PNG"/>

Nos aparecerá una nueva ventana, en el cual seleccionamos la pestaña CONTROLER SERVICES.
Veremos que tenemos un servicio llamado CDC MapCache, que es el cliente del operador ChangeCaptureMap. Tenemos que habilitar el servicio.
Hacemos clic en el icono de un rayo, para habilitarlo.

<img src="images/tapiz-2.PNG"/>

Nos aparecerá una nuba ventana, hacemos clic en ENABLE. Una vez habilitado, pulsamos CLOSED.

<img src="images/tapiz-3.PNG"/>

Ahora creamos el servicio servidor de MySQL. Hacemos clic en el icono +.

<img src="images/tapiz-4.PNG"/>

Nos aparecerá una nueva ventana, en el cual. buscamos DistributedMapCacheServer, seleccionamos y clic en ADD.
Esta acción, es añadir al tapíz, un servidor de caché, que guarda los registros que captura de MySQL.

<img src="images/tapiz-5.PNG"/>

Al igual que en el anterior servicio, tenemos que habilitarlo, clic en el rayo.

<img src="images/tapiz-6.PNG"/>

Nos aparecerá una nuba ventana, hacemos clic en ENABLE. Una vez habilitado, pulsamos CLOSED.

<img src="images/tapiz-7.PNG"/>

Lo único que nos falta es que el procesador, empiece a recoger los cambios del servidor MySQL. Clic derecho en el procesador CaptureChangeMySQL, y clic en Start.

<img src="images/tapiz-8.PNG"/>

En los conectores, vemos que ya están capturando cambios del servidor MySQL.

<img src="images/tapiz-9.PNG"/>



